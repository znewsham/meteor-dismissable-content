Package.describe({
  name: 'znewsham:dismissable-content',
  version: '0.1.1',
  // Brief, one-line summary of the package.
  summary: 'A simple package for creating user-dismissable content',
  // URL to the Git repository containing the source code for this package.
  git: 'git@bitbucket.org:znewsham/meteor-dismissable-content.git',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.4');
  api.use('ecmascript');
  api.use('mongo@1.1.14');
  api.use("blaze-html-templates@1.0.4");
  api.mainModule('dismissable-content-client.js',"client");
  api.mainModule('dismissable-content-server.js',"server");
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('znewsham:dismissable-content');
  api.mainModule('dismissable-content-tests.js');
});
