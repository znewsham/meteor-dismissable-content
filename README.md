# Dismissable Content
A simple package to allow for user-dismissable content.

## API
Stores a record of the dismissed content in a mongo collection "dismissables" with the userId, contentId and date dismissed.

## Usage
```html
<template name="my_template">
  {{#dismissable contentId="a-unique-content-id" class="some-class"}}
    My dismissable content
  {{/dismissable}}
</template>
```
