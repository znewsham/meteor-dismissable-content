import { BlazeLayout } from "meteor/kadira:blaze-layout";
import './dismissable.html';
import { Dismissables } from '../api/Dismissables.js';

Template.dismissable.events({
  "click .js-dismiss"(e){
    Dismissables.insert({
      userId: Meteor.userId(),
      contentId: Template.instance().data.contentId,
      dateDismissed: new Date()
    });
  }
});

Template.dismissable.helpers({
  visible(){
    return Template.instance().subscriptionsReady() && Dismissables.findOne({userId: Meteor.userId(), contentId: Template.instance().data.contentId}) === undefined;
  }
});

Template.dismissable.onCreated(function(){
  const self = this;
  this.autorun(function(){
    self.subscribe("dismissable", Meteor.userId(), self.data.contentId);
  });
});
