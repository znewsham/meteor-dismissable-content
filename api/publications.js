import { Dismissables } from './Dismissables.js';
Meteor.publish("dismissable", function(userId, contentId){
  return Dismissables.find({userId, contentId}, {fields: {contentId: true, userId: true}});
});
