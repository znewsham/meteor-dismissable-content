import { Dismissables } from './Dismissables.js';

Dismissables.allow({
    insert(userId, doc){
      return doc.userId == userId;
    },
    remove(){
      return false;
    },
    update(){
      return false;
    }
});
