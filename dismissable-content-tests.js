// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by meteor-dismissable-content.js.
import { name as packageName } from "meteor/znewsham:dismissable-content";

// Write your tests here!
// Here is an example.
Tinytest.add('dismissable-content - example', function (test) {
  test.equal(packageName, "dismissable-content");
});
